class CreateSensors < ActiveRecord::Migration[5.1]
  def change
    create_table :sensors do |t|
    	t.string :name
    	t.string :nameduino
    	t.text :description
    end
  end
end
