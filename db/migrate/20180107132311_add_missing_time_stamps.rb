class AddMissingTimeStamps < ActiveRecord::Migration[5.1]
  def change
    add_timestamps :sensors, null: true
    add_timestamps :users, null: true
  end
end
