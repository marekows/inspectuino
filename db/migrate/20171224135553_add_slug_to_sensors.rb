class AddSlugToSensors < ActiveRecord::Migration[5.1]
  def change
    add_column :sensors, :slug, :string
  end
end
