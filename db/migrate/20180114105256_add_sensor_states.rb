class AddSensorStates < ActiveRecord::Migration[5.1]
  def change
  	add_column :sensors, :low, :string
  	add_column :sensors, :high, :string
  	add_column :sensors, :divisor, :integer
  	add_column :sensors, :unit, :string
  end
end
