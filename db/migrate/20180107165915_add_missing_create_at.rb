class AddMissingCreateAt < ActiveRecord::Migration[5.1]
  def change
  	# Add timestamps column, but without a NOT NULL constraint

    # Backfill missing data with a really old date
    time = Time.zone.parse('2010-01-01 06:10:21')
    update "UPDATE sensors SET created_at = '#{time}'"
    update "UPDATE sensors SET updated_at = '#{time}'"

    # Restore NOT NULL constraints to be in line with the Rails default
    change_column_null :sensors, :created_at, false
    change_column_null :sensors, :updated_at, false
    
    # ...
  end
end
