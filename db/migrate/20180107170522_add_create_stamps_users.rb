class AddCreateStampsUsers < ActiveRecord::Migration[5.1]
  def change
  	  	# Add timestamps column, but without a NOT NULL constraint

    # Backfill missing data with a really old date
    time1 = Time.zone.parse('2014-04-12 06:18:21')
    time2 = Time.zone.parse('2015-02-22 02:33:53')
    update "UPDATE users SET created_at = '#{time1}'"
    update "UPDATE users SET updated_at = '#{time2}'"

    # Restore NOT NULL constraints to be in line with the Rails default
    change_column_null :users, :created_at, false
    change_column_null :users, :updated_at, false
    
    # ...
  end
end
