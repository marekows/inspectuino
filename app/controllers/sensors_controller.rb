class SensorsController < ApplicationController

before_action :restriction, only: [:index, :new, :create, :show, :edit, :update]
before_action :set_sensor, only: [:show, :edit, :update]
before_action :check_site_res, only: [:index, :show]
before_action :set_data, only: [:index, :show]
before_action :check_data_format, only: [:index, :show]

    def index

	    $list = @user.sensors

	end

	def new
			@sensor = Sensor.new
	end
 
	def create

		@sensor = Sensor.new(sensor_params)
		@sensor.user = current_user
		
		if @sensor.save
			redirect_to user_sensors_path
		else
			render 'new'
		end	
	end   


	def edit

		if @sensor.user == current_user
		else
		   redirect_to root_path
	    end

	end

	def update
        
		#@sensor.user = current_user
		if @sensor.update(sensor_params)
			redirect_to sensor_path(@sensor)
		else
			render 'edit'
		end

	end 

	def show

        if @sensor.user == current_user
		else
		redirect_to new_sensor_path 
		end
		@page_title = @sensor.name
	end
	

	private

	def sensor_params
		params.require(:sensor).permit(:name, :nameduino, :description, :location_id, :low, :high, :divisor, :unit)
	end

	def set_data

	if @code.to_i == 200 or @code.to_i == 302
		        data = Nokogiri::HTML(open(@user.IP))
				$data2 = data.text.split(" ")
			else 
				redirect_to edit_user_registration_path 
			end
	end


	def check_site_res

		 	begin
		     
		    res = Net::HTTP.get_response(URI(@user.IP))
		     
		    rescue 
		      @code=res.code
		    else
		      @code=res.code

		    ensure

		    return @code
		   
		    end
 
	end

	def restriction

		if user_signed_in? 
		@user = current_user
		else
         #redirect_to :controller => 'users', :action => 'new'
         redirect_to new_user_session_path  
      	end


	end


	def check_data_format 
    
	end

	def set_sensor
	@sensor = Sensor.friendly.find(params[:id])
	end

end
