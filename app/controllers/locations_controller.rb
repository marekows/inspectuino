class LocationsController < ApplicationController

before_action :restriction, only: [:index, :new, :create, :show]
before_action :set_location, only: [:show, :edit, :update]

    def index

	    $list = @user.locations

	end

	def new
		@location = Location.new
	end
 
	def create

		@location = Location.new(location_params)
		@location.user = current_user
		
		if @location.save
			redirect_to add_sensor_path
		else
			render 'new'
		end	
	end   


	def edit
   	
		if @location.user == current_user
		else
		   redirect_to root_path
	    end

	end

	def update
        
		#@location.user = current_user
		if @location.update(location_params)
			redirect_to location_path(@location)
		else
			render 'edit'
		end

	end 

	def show

        if @location.user == current_user
		else
		redirect_to new_location_path 
		end
		@page_title = @location.name
	end
	

	private

	def location_params
		params.require(:location).permit(:name, :description)
	end


	def restriction

		if user_signed_in? 
		@user = current_user
		else
         #redirect_to :controller => 'users', :action => 'new'
         redirect_to new_user_session_path  
      	end


	end

	def set_location
	@location = Location.friendly.find(params[:id])
	end

end
