class Sensor < ApplicationRecord
	validates :name, presence: true
	validates :nameduino, presence: true
	belongs_to :user
	belongs_to :location

	extend FriendlyId
    friendly_id :name, use: :slugged

    after_create :remake_slug

  # Try building a slug based on the following fields in
  # increasing order of specificity.
  def slug_candidates
    [
      :name,
      [:name, :id],
    ]
  end

  def remake_slug
    self.update_attribute(:slug, nil)
    self.save!
  end

  #You don't necessarily need this bit, but I have it in there anyways
  def should_generate_new_friendly_id?
    new_record? || self.slug.nil?
  end
  
end