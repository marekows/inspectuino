class Location < ApplicationRecord

validates :name, presence: true
has_many :sensors
belongs_to :user

extend FriendlyId
friendly_id :name, use: :slugged

end