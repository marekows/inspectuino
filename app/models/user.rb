class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
	validates :name, presence: true
	has_many :sensors
	has_many :locations

	after_create :init_profile

	def init_profile
    	Location.create(name: "none", user_id: User.last.id)
  end

end