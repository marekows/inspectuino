module ApplicationHelper

	def login_helper style

		if current_user
	  	   (link_to "Logout", destroy_user_session_path, method: :delete, class: style)+ " ".html_safe +
	       (link_to "My Account", user_account_path, class: style)+ " ".html_safe +
	       (link_to "Edit My Account", edit_user_registration_path, class: style)
		else
			(link_to "Login", new_user_session_path, class: style)+ " ".html_safe +
			(link_to "Register", new_user_registration_path, class: style)
		end
	end

	def restriction_helper style
		if current_user
	       (link_to "My Sensors", user_sensors_path, class: style)+'<li class="nav-item">'.html_safe +
	       (link_to "Add Sensor", add_sensor_path, class: style)+'<li class="nav-item">'.html_safe +
	       (link_to "Add Location", add_location_path, class: style)
		end
	end
end
