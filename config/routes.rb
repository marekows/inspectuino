Rails.application.routes.draw do


  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register'} 
	root "sensors#index"
	get '/about', to: 'pages#about', as: 'about'
	
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :sensors, except: [:index]
  get 'my-sensors', to: 'sensors#index', as: 'user_sensors'
  get 'sensor/:id', to: 'sensors#show', as: 'sensor_show'

  resources :users, except: [:new, :edit, :destroy]
  get 'my-account', to: 'users#show', as: 'user_account'
  get 'add-sensor', to: 'sensors#new', as: 'add_sensor'
  
  resources :locations, except: [:index]
  get 'location/:id', to: 'locations#show', as: 'location_show'
  get 'add-location', to: 'locations#new', as: 'add_location'
end

